#ifndef __UTIL__
#define __UTIL__

enum{SIZE=4096};

/*Prototipe of functions*/
char *cleanDigits(char *data);
char *cleanAmount(char *data);
char *cleanCard(char *data);
void lineFile(char *buffer);
void rightZero(char *buffer, int total);
void leftZero(char *buffer, int total);
bool ValidateCard(char *card, int &id);
bool FindLabel(int id);
unsigned char *convertToASCII(char *word);
char *toChar(unsigned char *wordASCII);
void analizeResponse(unsigned char *word);

/*structuras*/
typedef struct {
    char rangeLow[9];
    char rangeHigh[9];
    unsigned char len;
    int id;
} range_t;

typedef struct {
    char label[13];
    int id;
} card_t;

#endif