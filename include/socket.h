#ifndef __SOCKET__
#define __SOCKET__

/*Socket*/
int socketCreate();
int socketConnect(int handle, const char * ip, unsigned short port);
int socketRead(int handle, unsigned char *&data, int maxTimeout);
int socketWrite(int handle, const unsigned char * data);
int socketClose(int handle);

#endif