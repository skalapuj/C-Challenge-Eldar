#include "../include/heads.h"

char *cleanDigits(char *data){
    char *result =(char *) malloc(strlen(data)+1);
    int i=0;
    if(!result)
        return NULL;

    while(*data != '\0'){
        if(isdigit(*data) != 0 )
            result[i++]=*data;
        data++;
    }
    result[i]='\0';
    return result;
}

char *cleanCard(char *data){
    char  *result; 
    int i=0;
     if(!(result=(char *)malloc(strlen(data)+1))){
        return NULL;
    }

    while(*data != '\0'){
        if(isdigit(*data) != 0){
            result[i++]=*data;
        }
        data++;
    }
    result[i]='\0';
    if(strlen(result)<13){
        free(result);
        printf("ERROR, the length of card is wrong\n"); 
        return NULL;
    }

    return result;
}

unsigned char *convertToASCII(char *word){
    unsigned char *wordASCII;
    char  aux[100];
    if(!(wordASCII=(unsigned char *)malloc(1024)))
        return NULL;
    strcpy((char *)wordASCII,"");
    for(int i=0;*word != '\0';i++){
        sprintf(aux,"%d ",*word);
        strcat((char *)wordASCII, (char *)aux);
        word++;
    } 
    return wordASCII;
}

char *toChar(unsigned char *wordASCII){
    char *word;
    char  aux[100];
    if(!(word=(char *)malloc(1024)))
        return NULL;
    for(int i=0;*wordASCII != '\0';i++){
        sprintf(aux,"%c",*wordASCII);
        strcat(word, aux);
        wordASCII++;
    }
    return word;
}

char *cleanAmount(char *data){
    char  *result, decimals[3], integers[11];
    int   i=0, j=0;
    bool  decimal=false;
    strcpy(decimals,"");
    strcpy(integers,"");
    if(!(result=(char *)malloc(13)))
        return NULL;

    while(*data != '\0' && *data!=',' && *data !='.' && i<10){
        if(isdigit(*data) != 0){
            integers[i++]=*data;
        }
        data++;
    }
    integers[i]='\0';

    if(*data==',' || *data =='.'){
        decimal=true;
        data++;
    }
    else{
        if(*data != '\0')
            printf("Warning, the amount could be wrong\n");        
    }

    while(decimal && j<2 && *data != '\0'){
        if(isdigit(*data) != 0){
            decimals[j++]=*data;
        }
        data++;
    }
    leftZero(integers,10);
    rightZero(decimals,2);
    sprintf(result, "%s%s",integers,decimals);
    result[12]='\0';
    return result;
}

void lineFile(char *buffer){
    for(int j=strlen(buffer)-1; j<10; j++)
        strcat(buffer,"0");
    buffer[10]='\r';
    buffer[11]='\n';
    buffer[12]='\0';
}

void rightZero(char *buffer, int total){
    while(strlen(buffer)<total){
        strcat(buffer,"0");
    }
}

void leftZero(char *buffer, int total){
    char aux[1024];
    while(strlen(buffer)< total){
        strcpy(aux, buffer);
        sprintf(buffer,"0%s",aux);
    }
}

void analizeResponse(unsigned char *word){
    char *decodeResponse = toChar(word);
    char dest[7];
    if(strlen(decodeResponse)<6){
        printf("RECHAZADA");
        goto FINAL;
    }
    strncpy(dest, decodeResponse+strlen(decodeResponse)-6, 6);
    dest[6]='\0';
    if(strcmp(dest,(char *)"48 48 ")==0)
        printf("APROBADA\n");
    else
       printf("RECHAZADA\n");
    
    FINAL:
    free(decodeResponse);
}

bool ValidateCard(char *card, int &id){
    char            range[9], aux[1024];
    unsigned int    lenCard, rangeCard, RangeMin, RangeMax, lenDat;
    range_t         rangeDat; 
    FILE            *file;
    bool            foundId=false;
    strncpy(range,card,8);
    rangeCard=atoi(range);
    lenCard = strlen(card);
    
    file = fopen("data/range.dat","r");
    while(fread(&rangeDat, sizeof(rangeDat), 1, file) && !foundId){
        RangeMin=atoi((char *)rangeDat.rangeLow);
        RangeMax=atoi((char *)rangeDat.rangeHigh);
        lenDat=rangeDat.len;
        id=rangeDat.id;

        if(lenDat==lenCard && RangeMin <= rangeCard  && RangeMax >= rangeCard)
            foundId=true;
    } 
    fclose(file);

    return foundId;
}

bool FindLabel(int id){
    card_t          cardDat; 
    FILE            *file;
    bool            foundId=false;
    
    file = fopen("data/cards.dat","r");
    while(fread(&cardDat, sizeof(cardDat), 1, file) && !foundId){
        if(id==cardDat.id){
            printf("%s\n",cardDat.label);   
            foundId=true;
        }
    } 
    fclose(file);

    return foundId;
}