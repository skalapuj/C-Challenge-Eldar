#include "../include/heads.h"
#define DEBUG 0

int main(){
    unsigned char *responseMessage, *requestMessageASCII;
    unsigned short port = 8080;
    char    element[SIZE], amount[13], card[101], securityCode[4], lenCard[3], requestMessage[122], ipconnection[20], *aux;
    int     id, handle, timeout = 5000, close;
    int     readBytes, writtenBytes, connection;
    strcpy(ipconnection,"0.0.0.0");
    
    /*Get amount*/
    printf("Enter your amount: ");
    scanf("%s", (char *)&element);/*This shouldn't get letters and its size should be less than 4 kb*/
   
    while(getchar()!='\n');/*clean buffer*/

    if((aux=cleanAmount(element))== NULL){
        printf("Error reading amount\n");
        goto FINAL;
    } 
    strncpy(amount, aux, 12);
    amount[strlen(aux)]='\0';
    free(aux);

    #if DEBUG
        printf("Amount=%s\n", amount);
    #endif
    
    /*Get card number*/
    printf("Enter your card number: ");
    scanf("%s", (char *)&element);/*This shouldn't get letters and its size should be less than 4 kb*/
    
    while(getchar()!='\n');/*clean buffer*/
    
    if((aux=cleanCard(element))== NULL){
        printf("Error reading card number\n");
        goto FINAL;
    } /*Clean data*/
    strncpy(card,aux,100);
    card[strlen(aux)]='\0';
    free(aux);

    sprintf(lenCard, "%ld", strlen(card));
    leftZero(lenCard,2);

    #if DEBUG
        printf("cardNumber=%s and len= %s\n", card, lenCard);
    #endif
   
    /*Validate card*/
    if(!ValidateCard(card, id)){
        printf("TARJETA NO SOPORTADA\n");
        goto FINAL;
    } 

    #if DEBUG
        printf("tarjeta valida id=%d\n", id);
    #endif

    /*Validate label 4 id*/
    if(!FindLabel(id)){
        printf("Error, not found label for this range\n");
        goto FINAL;
    } 

    /*get security code*/
    printf("Enter your security code: ");
    scanf("%s", (char *)&element);/*This shouldn't get letters and its size should be less than 4 kb*/

    while(getchar()!='\n');/*clean buffer*/

    if((aux=cleanDigits(element))== NULL){
        printf("Error reading security code\n");
        goto FINAL;
    }
    strncpy(securityCode,aux,3);
    securityCode[3]='\0';
    free(aux);

    #if DEBUG
        printf("security code: %s\n",securityCode);
    #endif
    
    /*Conect socket*/
    sprintf(requestMessage, "0200%s%s%s%s", lenCard, card, amount, securityCode);
    if((requestMessageASCII=convertToASCII(requestMessage))== NULL){
        printf("Error reading security code\n");
        goto FINAL;
    }
    #if DEBUG
        printf("requestMessage: %s y en ascii %s\n",requestMessage, requestMessageASCII);
    #endif
    
    if((handle = socketCreate()) == -1){
        printf("Error couldn't create Socket");
        goto FINAL;
    }

    #if DEBUG
        printf("Socket created\n");
    #endif

    if((connection = socketConnect(handle, ipconnection, port)== -1)){
        printf("Error couldn't connect Socket");
        if((close = socketClose(handle)) == -1)
            printf("Error: Couldn't close Socket");
        goto FINAL;
    }

    #if DEBUG
        printf("Socket connected\n");
    #endif

    if((writtenBytes = socketWrite(handle, requestMessageASCII)) == -1){
        free(requestMessageASCII);
        printf("Error couldn't write in Socket");
        if((close = socketClose(handle)) == -1)
            printf("Error couldn't close Socket");
        goto FINAL;
    }
    free(requestMessageASCII);

    #if DEBUG
        printf("Socket written\n");
    #endif
                    
    if((readBytes = socketRead(handle, responseMessage, timeout)) == -1){
        printf("ERROR DE COMUNICACION");
        if((close = socketClose(handle)) == -1)
            printf("Error: Couldn't close Socket");
        goto FINAL;
    }
    #if DEBUG
        printf("Socket read\n");
    #endif

    if((close = socketClose(handle)) == -1)
        printf("Error: Couldn't close Socket");

    #if DEBUG
        printf("Socket closed\n");
    #endif

    analizeResponse(responseMessage);
    free(responseMessage);  

    FINAL:
    return 0;
} 