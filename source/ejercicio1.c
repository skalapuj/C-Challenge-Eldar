#include "../include/heads.h"
#define DEBUG 0

int main(){
    char    element[SIZE], *digitData, filename[50], buffer[13];
    FILE    *fp; 
    
    printf("Enter your number, remember this finish with zero: ");
    scanf("%[^0]", (char *)&element);/*This shouldn't get letters and its size should be less than 4 kb*/

    while(getchar()!='\n');/*clean buffer*/

    if((digitData=cleanDigits(element))== NULL){
        printf("Error reading your input\n");
        goto FINAL;
    } /*Clean data*/
   
    printf("Enter your filename: ");
    scanf("%s", (char *)&filename);/*This size should be less than 50 b*/

    fp = fopen(filename, "w");
    if (fp == NULL){
        free(digitData);
        printf("Error create the file %s\n", filename);
        goto FINAL;
    }/*open archive*/    

    for (int i = 0; i < strlen(digitData)/10 + ((strlen(digitData)%10)? 1: 0); i++){
        strncpy(buffer, (digitData + i*10), 10);
        lineFile(buffer);
        #if DEBUG
            printf("%s", buffer);
        #endif
        fprintf(fp, "%s", buffer);
    }
    fclose(fp);

    free(digitData);
    printf("File %s writen succesfuly.\n", filename);    

    FINAL:
    return 0;
} 