#include "../include/heads.h"

int main (int argc, char*argv[]) {  
    FILE *outfile; 
    char filename[50], struct_[20];
    int  a=21;
    unsigned char i = a;
    
    if(argc < 3){
        printf("El modo de uso es ./ejecutable <nombre de la estructura> <nombre del archivo a generar> por ejemplo './createDat.o card_t cards.dat\n");
        goto FINAL;
    }
    strcpy(struct_,  argv[1]);
    strcpy(filename, argv[2]);
    printf("%s\n",struct_);

    outfile = fopen(filename, "w"); 

    if(strcmp(struct_,"card_t")==0){
        card_t input1 = {"michifeliz01", 4}; 
        card_t input2 = {"michifeliz02", 2}; 
        card_t input3 = {"michifeliz03", 3}; 
        // write struct to file 
        fwrite(&input1, sizeof(card_t), 1, outfile); 
        fwrite(&input2, sizeof(card_t), 1, outfile); 
        fwrite(&input3, sizeof(card_t), 1, outfile); 
    }else if(strcmp(struct_,"range_t")==0){        
        range_t input1  = {"10000000", "30000000", i, 1}; 
        range_t input2  = {"30000000", "40000000", i, 2}; 
        range_t input3  = {"40000000", "50000000", i, 3}; 
        // write struct to file 
        fwrite(&input1, sizeof(range_t), 1, outfile); 
        fwrite(&input2, sizeof(range_t), 1, outfile); 
        fwrite(&input3, sizeof(range_t), 1, outfile); 
    } else {
        printf("Estructura desconocida\n");
        fclose (outfile); 
        goto FINAL;
    }
    // close file 
    fclose (outfile); 
    printf("archivo generado exitosamente\n");
    FINAL:
    return 0; 
}